using System;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;

public class AuraTexture : MonoBehaviour
{
    private Texture2D _texture;
    [SerializeField] private Texture2D _baseTexture;
    private CommandBuffer _command;
    private bool _inversed;
    private bool _lastInversed;
    private MaterialPropertyBlock _prop;
    [SerializeField] private SceneCharacter _character;
    private float inversedTime;
    [SerializeField] private Material _mainMaterial;
    [SerializeField] private Material _inversedMaterial;
    private Renderer _renderer;
    private static readonly int MainTex = Shader.PropertyToID("_MainTex");
    private static readonly int MaskTex = Shader.PropertyToID("_MaskTex");

#if PLATFORM_IOS
    [System.Runtime.InteropServices.DllImport("__Internal")]
#else
    [DllImport("Plasma")]
#endif
    private static extern IntPtr GetTextureUpdateCallback();

    private void Start()
    {
        _command = new CommandBuffer();
        _texture = new Texture2D(64, 64, TextureFormat.RGBA32, false);
        _texture.wrapMode = TextureWrapMode.Clamp;
        _mainMaterial.SetTexture(MainTex, _texture);
        _inversedMaterial.SetTexture(MaskTex, _texture);
        _renderer = GetComponent<Renderer>();
    }


    private void OnEnable()
    {
        if (!_character)
        {
            Debug.LogWarning("No character!", this);
            return;
        }

        _character.CharacterDoAction += OnCharacterDoAction;
    }

    private void OnCharacterDoAction()
    {
        inversedTime = 2;
    }

    private void OnDisable()
    {
        if (!_character) return;

        _character.CharacterDoAction -= OnCharacterDoAction;
    }

    private void OnDestroy()
    {
        _command.Dispose();
        Destroy(_texture);
    }

    private void Update()
    {
        if (inversedTime > 0)
        {
            _inversed = true;
            inversedTime -= Time.deltaTime;
        }
        else
        {
            _inversed = false;
        }


        if (_lastInversed != _inversed) changeState(_inversed);

        // Request texture update via the command buffer.
        _command.IssuePluginCustomTextureUpdateV2(
            GetTextureUpdateCallback(), _texture, (uint) (Time.time * 60)
        );
        Graphics.ExecuteCommandBuffer(_command);
        _command.Clear();
        _lastInversed = _inversed;
    }

    private void changeState(bool inversed)
    {
        _renderer.material = inversed ? _inversedMaterial : _mainMaterial;
    }
}