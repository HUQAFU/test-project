﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RotateAround : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _rotationSpeed = 5f;
    [SerializeField] private float _radius = 3f;
    float _angle;

    // Update is called once per frame
    void Update()
    {
        _angle += _rotationSpeed * Time.deltaTime;
        var offset = new Vector3(Mathf.Sin(_angle), 0, Mathf.Cos(_angle)) * _radius;
        transform.position = _target.position + offset;
    }
}