﻿using System;
using UnityEngine;

public class Energy
{
    private readonly float _max;
    private readonly float _perSecondRestoration;
    private float _count;
    private bool _restoring;
    private float _waitTime;


    public Energy(float maxEnergy, float perSecondRestoration)
    {
        _max = maxEnergy;
        _count = _max;
        _perSecondRestoration = perSecondRestoration;
        UpdateProvider.OnUpdate += Update;
        CountChange += OnCounntChange;
    }

    public float Normalized => _count / _max;

    public bool Restoring
    {
        get { return _restoring; }
        private set
        {
            _restoring = value;
            if (_restoring) ReadyForRestoration = false;
        }
    }

    public bool ReadyForRestoration { get; private set; }

    public float Count
    {
        get { return _count; }
        private set
        {
            var increasing = value > _count;
            _count = value;
            CountChange?.Invoke(_count, increasing);
        }
    }

    public event Action NotEnough;
    public event Action<float, bool> CountChange;

    private void OnCounntChange(float count, bool increasing)
    {
        if (Restoring && count == _max || !increasing) Restoring = false;

        ReadyForRestoration = count <= 0 && !Restoring;
    }

    public void StartRestoration()
    {
        if (!ReadyForRestoration)
            Debug.LogWarning("StartRestoration is only avaliable on zero energy!");
        else
            Restoring = true;
    }

    private void Update()
    {
        if (Restoring && _perSecondRestoration > 0)
        {
            _waitTime += Time.deltaTime;
            if (_waitTime >= 1)
            {
                var value = Count + _perSecondRestoration;
                Count = Mathf.Clamp(value, 0, _max);
                _waitTime = 0;
            }
        }
        else
        {
            _waitTime = 0;
        }
    }

    public bool TryToSpendEnergy(float spendCount)
    {
        if (Count >= spendCount)
        {
            Count -= spendCount;
            return true;
        }

        NotEnough?.Invoke();
        return false;
    }
}