using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessController : MonoBehaviour
{
    [SerializeField] private SceneCharacter _character;
    [SerializeField] private PostProcessLayer _postProcessLayer;
    private float _postProcessTime;


    void OnEnable()
    {
        if (!_character)
        {
            Debug.LogWarning("No character!", this);
            return;
        }

        _character.CharacterDoAction += OnCharacterDoAction;
    }

    private void OnCharacterDoAction()
    {
        _postProcessTime = 2;
    }

    void OnDisable()
    {
        if (!_character)
        {
            return;
        }

        _character.CharacterDoAction -= OnCharacterDoAction;
    }


    void Update()
    {
        if (_postProcessTime > 0)
        {
            _postProcessLayer.enabled = true;
            _postProcessTime -= Time.deltaTime;
        }
        else
        {
            _postProcessLayer.enabled = false;
        }

    }
}