﻿using System;
using UnityEngine;

public class UpdateProvider : MonoBehaviour
{
    public static Action OnUpdate;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CreateInstanceBinder()
    {
        var go = new GameObject("UpdateProvider");
        DontDestroyOnLoad(go);
        go.AddComponent<UpdateProvider>();
    }

    private void Update()
    {
        OnUpdate?.Invoke();
    }
}