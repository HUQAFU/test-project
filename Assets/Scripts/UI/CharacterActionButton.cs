﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class CharacterActionButton : MonoBehaviour
{
    private Button _button;
    string _characterId;
    int _actionId;

    [SerializeField] private Text _actionTitle;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(RequestAction);
    }

    public void SetupButton(string characterId, int actionId, CharacterAction characterAction)
    {
        _characterId = characterId;
        _actionId = actionId;
        _actionTitle.text = $"{characterAction.Animation.name}  ({characterAction.CnergyCost})";
    }

    // Update is called once per frame
    public void RequestAction()
    {
        ActionManager.RequestAction(_characterId, _actionId);
    }
}