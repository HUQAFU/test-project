﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SliderEnergyShower : MonoBehaviour
{
    private Slider _slider;
    [SerializeField] private SceneCharacter _character;

    private Color _baseColor;

    // Start is called before the first frame update
    private void Awake()
    {
        _slider = GetComponent<Slider>();
        _baseColor = _slider.colors.disabledColor;
    }

    void OnEnable()
    {
        if (!_character)
        {
            Debug.LogWarning("No character!", this);
            return;
        }
        _character.Energy.CountChange += EnergyOnCountChange;
        _character.Energy.NotEnough += EnergyOnNotEnough;
        EnergyOnCountChange(_character.Energy.Count, false);
    }

    void OnDisable()
    {
        if (!_character)
        {
            return;
        }

        _character.Energy.CountChange -= EnergyOnCountChange;
        _character.Energy.NotEnough -= EnergyOnNotEnough;
    }

    private Coroutine NotEnoughAnimationCoroutine;

    private void EnergyOnNotEnough()
    {
        if (NotEnoughAnimationCoroutine != null)
        {
            StopCoroutine(NotEnoughAnimationCoroutine);
            NotEnoughAnimationCoroutine = null;
        }

        NotEnoughAnimationCoroutine = StartCoroutine(NotEnoughAnimation());
    }

    private void EnergyOnCountChange(float energy, bool increasing)
    {
        var colors = _slider.colors;
        colors.disabledColor = _character.Energy.Restoring? Color.cyan : _baseColor;
        _slider.colors = colors;
        _slider.value = _character.Energy.Normalized;
    }

    IEnumerator NotEnoughAnimation()
    {
        var colors = _slider.colors;
        colors.disabledColor = Color.red;
        _slider.colors = colors;
        yield return new WaitForSeconds(1);
        colors.disabledColor = _baseColor;
        _slider.colors = colors;
    }
}