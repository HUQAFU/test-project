﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GenerateActionButtons : MonoBehaviour
{
    [SerializeField] private GameObject _actionButtonPrefab;

    [SerializeField] private SceneCharacter _sceneCharacter;
    private Character _character;
    // Start is called before the first frame update
    void Start()
    {
        if (!_sceneCharacter)
        {
            Debug.LogWarning("Set _character!", this);
        }
        var characterList = Resources.Load<CharacterList>("CharacterList");
        _character = characterList.Characters.First(x => x.CharacterId == _sceneCharacter._characterId);

        foreach (var characterAction in _character.CharacterActions)
        {
            var button = Instantiate(_actionButtonPrefab, transform);
            var characterActionButton = button.GetComponent<CharacterActionButton>();
            if (!characterActionButton)
            {
                Debug.LogWarning("Missed CharacterActionButton component in _actionButtonPrefab!", this);
                break;
            }

            characterActionButton.SetupButton(_character.CharacterId, characterAction.Key, characterAction.Value);  
        }
    }
}
