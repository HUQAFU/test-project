﻿using System;
using System.Linq;
using UnityEngine;

public class ActionManager
{
    public static event Action<string, int> CharacterActionRequested;

    public static void RequestAction(string characterId, int actionId)
    {   
       CharacterActionRequested?.Invoke(characterId, actionId);
    }
}
