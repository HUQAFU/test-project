﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;


[System.Serializable]
public class CharacterAction
{
    [Required]
    public AnimationClip Animation;
    public float CnergyCost = 10;
}



[System.Serializable]
public class Character
{
    public string CharacterId = "character";
    public float MaxEnergy = 100;
    public float PerSecondRestoration = 1;
    public AnimationClip Idle;
    public AnimationClip RecoveryStart;
    public AnimationClip WakeUp;
    public Dictionary<int, CharacterAction> CharacterActions = new Dictionary<int, CharacterAction>();
    
}

[CreateAssetMenu(fileName = "CharacterList", menuName = "ScriptableObjects/CharacterList", order = 1)]
public class CharacterList : SerializedScriptableObject
{
    [OdinSerialize]
    Character[] _characters = new Character[0];

    public Character[] Characters
    {
        get { return _characters; }
    }
}


