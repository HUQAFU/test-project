﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

public class SceneCharacter : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [ValueDropdown("CharacterIds")] public string _characterId;
    private readonly int _actionStringHash = Animator.StringToHash("Action");
    private readonly int _breakRestorationStringHash = Animator.StringToHash("BreakRestoration");
    private readonly int _canStartRestoreStringHash = Animator.StringToHash("CanStartRestore");
    private readonly int _inActionStringHash = Animator.StringToHash("InAction");
    private readonly int _needBrakeRestorationStringHash = Animator.StringToHash("NeedBrakeRestoration");
    private readonly int _restoringStringHash = Animator.StringToHash("Restoring");
    private readonly int _secondActionStringHash = Animator.StringToHash("SecondAction");
    private Character _character;
    private CharacterList _characterList;
    private AnimatorOverrideController animatorOverrideController;
    public Action CharacterDoAction;
    public Energy Energy;

    // Start is called before the first frame update
    private void Awake()
    {
        _characterList = Resources.Load<CharacterList>("CharacterList");
        _character = _characterList.Characters.First(x => x.CharacterId == _characterId);
        Energy = new Energy(_character.MaxEnergy, _character.PerSecondRestoration);
        Energy.CountChange += EnergyOnCountChange;
        SetCharacterAnimationsAndParams();
    }

    private void Update()
    {
        _animator.SetBool(_restoringStringHash, Energy.ReadyForRestoration || Energy.Restoring);

        if (_animator.GetBool(_canStartRestoreStringHash) && Energy.ReadyForRestoration)
        {
            Energy.StartRestoration();
            _animator.SetBool(_canStartRestoreStringHash, false);
        }
    }

    private void SetCharacterAnimationsAndParams()
    {
        animatorOverrideController = new AnimatorOverrideController(_animator.runtimeAnimatorController);
        animatorOverrideController["Idle"] = _character.Idle;
        animatorOverrideController["RestoringStart"] = _character.RecoveryStart;
        animatorOverrideController["WakeUp"] = _character.WakeUp;
        _animator.runtimeAnimatorController = animatorOverrideController;
    }
    
    private void EnergyOnCountChange(float count, bool increasing)
    {
        if (_animator.GetBool(_needBrakeRestorationStringHash) && Energy.Normalized == 1) _animator.SetTrigger(_breakRestorationStringHash);
    }

    private void OnEnable()
    {
        ActionManager.CharacterActionRequested += ActionRequestedManagerOnCharacterActionRequested;
    }

    private void OnDisable()
    {
        ActionManager.CharacterActionRequested -= ActionRequestedManagerOnCharacterActionRequested;
    }

    private void ActionRequestedManagerOnCharacterActionRequested(string characterId, int actionId)
    {
        if (_characterId != characterId) return;

        var action = _character.CharacterActions[actionId];
        if (action != null)
            if (Energy.TryToSpendEnergy(action.CnergyCost))
            {
                var secondAction = _animator.GetBool(_secondActionStringHash);
                if (_animator.GetBool(_inActionStringHash)) secondAction = !secondAction;
        
                var animationId = secondAction ? "Action2" : "Action";
                animatorOverrideController[animationId] = action.Animation;     
                _animator.SetBool(_actionStringHash, true);
                if (_animator.GetBool(_needBrakeRestorationStringHash))
                    _animator.SetTrigger(_breakRestorationStringHash);
                
                CharacterDoAction?.Invoke();
            }
    }
#if UNITY_EDITOR
    private string[] CharacterIds()
    {
        var list = (CharacterList) AssetDatabase.LoadAssetAtPath("Assets/Resources/CharacterList.asset",
            typeof(CharacterList));
        var character = list.Characters.Select(x => x.CharacterId);
        return character.ToArray();
    }
#endif
}