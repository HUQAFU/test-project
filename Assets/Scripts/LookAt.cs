﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LookAt : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private bool _cameraFacting;

    // Update is called once per frame
    void Update()
    {
        if (_cameraFacting)
        {
            transform.LookAt(transform.position + _target.rotation * Vector3.forward,
                _target.rotation * Vector3.up);
        }
        else
        {
            transform.LookAt(_target);
        }
    }
}