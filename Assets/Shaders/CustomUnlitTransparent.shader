﻿Shader "Unlit/CustomUnlitTransparent"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _MainTexScale("UV Scale", Range(0,10)) = 1
        _MaskTex ("Mask Texture", 2D) = "white" {}
        _MaskTexScale("UV Mask Scale", Range(0,10)) = 1
        _FinalMask ("Final Mask Texture", 2D) = "white" {}
    }
    SubShader
    {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
    
        LOD 100
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha 
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
		        float4 vertex : POSITION;
		        float3 normal : NORMAL; 
		        float2 uv : TEXCOORD0;
	        };

	        struct v2f {
		        float4 pos : POSITION;
		        half3 objNormal : TEXCOORD0;
		        float3 coords : TEXCOORD1;
                float2 uv : TEXCOORD2;
	        };

            sampler2D _MainTex;
            float _MainTexScale;
            sampler2D _MaskTex;
            float _MaskTexScale;
            sampler2D _FinalMask;

	        float2 GetScreenUV(float2 clipPos, float UVscaleFactor)
	        {
		        float4 SSobjectPosition = UnityObjectToClipPos (float4(0,0,0,1.0)) ;
		        float2 screenUV = float2(clipPos.x/_ScreenParams.x,clipPos.y/_ScreenParams.y);
		        float screenRatio = _ScreenParams.y/_ScreenParams.x;

		        screenUV.y -=0.5;
		        screenUV.x -=0.5;

		        screenUV.x -= SSobjectPosition.x/(2*SSobjectPosition.w);
		        screenUV.y += SSobjectPosition.y/(2*SSobjectPosition.w);
		        screenUV.y *= screenRatio;

		        screenUV *= 1/UVscaleFactor;
		        screenUV *= SSobjectPosition.z;

		        return screenUV;
	        };
	
		    v2f vert(appdata v) {				
			    v2f o;
			    o.pos = UnityObjectToClipPos(v.vertex);
                o.coords = v.vertex.xyz;
                o.objNormal = v.normal;
                o.uv = v.uv;
			    return o;
		    }		

		    half4 frag(v2f i) :COLOR 
		    { 				
			    float2 screenUV = GetScreenUV(i.pos.xy, _MainTexScale);
			    float2 maskScreenUV = GetScreenUV(i.pos.xy, _MaskTexScale);
			    half4 screenTexture = tex2D (_MainTex, screenUV);
                half4 maskTexture = tex2D (_MaskTex, maskScreenUV);
                half4 finalMaskTexture = tex2D (_FinalMask, i.uv);
            
                fixed4 resultCol = fixed4(screenTexture.r, screenTexture.g, screenTexture.b, maskTexture.r);
			    return fixed4(resultCol.r, resultCol.g, resultCol.b, resultCol.a * finalMaskTexture.a); 
		    } 
            ENDCG
        }
    }
}
